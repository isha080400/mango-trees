import java.util.Scanner;

public class MangoTreeFinder {
    public static void isMangoTree() {
        Scanner sc = new Scanner(System.in);

        int numOfRows, numOfColumns, positionOfTree, flag = 0;

        numOfRows = sc.nextInt();
        numOfColumns = sc.nextInt();
        positionOfTree = sc.nextInt();

        if(positionOfTree >= 1 && positionOfTree <= numOfColumns){
            flag = 1;
        }
        else if((positionOfTree - 1) % numOfColumns == 0){
            flag = 1;
        }
        else if(positionOfTree % numOfColumns == 0){
            flag = 1;
        }

        if(flag == 1){
            System.out.println("yes");
        }
        else{
            System.out.println("no");
        }

    }

    public static void main(String[] args){
        isMangoTree();
    }
}
